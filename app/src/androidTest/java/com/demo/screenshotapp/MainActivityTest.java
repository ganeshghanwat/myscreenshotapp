package com.demo.screenshotapp;

import android.graphics.Bitmap;
import android.os.Handler;
import android.support.test.annotation.UiThreadTest;
import android.support.test.filters.MediumTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import static com.demo.screenshotapp.Utils.saveScreenShotToStorage;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import static com.demo.screenshotapp.Utils.captureScreenShot;

/**
 * Created by Ganesh on 4/15/2018.
 */

@MediumTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

@Rule
public ActivityTestRule<MainActivity> rule = new ActivityTestRule<MainActivity>(MainActivity.class);


    @Test
    public void ensureScreenShotIsCaptured(){

        MainActivity mainActivity = rule.getActivity();

       final View root_view = mainActivity.findViewById(R.id.activity_main);

        assertThat(root_view,notNullValue());

        Bitmap bitmap = captureScreenShot(root_view);

        assertNotNull("screenshot is null",bitmap);

    }

    @Test
    public void ensureScreenshotSavedToLocalStorage(){

        MainActivity mainActivity = rule.getActivity();

        final View root_view = mainActivity.findViewById(R.id.activity_main);

        assertThat(root_view,notNullValue());

        Bitmap bitmap = captureScreenShot(root_view);

        assertNotNull("screenshot is null",bitmap);

        File file = saveScreenShotToStorage(bitmap);

        assertNotNull("screenshot is not saved to local storage",file);

    }

}
