package com.demo.screenshotapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.demo.screenshotapp.Constants.SCREENSHOT_INTERVAL;
import static com.demo.screenshotapp.Constants.SCREENSHOT_LIMIT;
import static com.demo.screenshotapp.Utils.resumeFailedUploads;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button btClose;
    RelativeLayout root_view;
    CoordinatorLayout coordinatorLayout;
    Snackbar snackbar;

    int screenShotCount;
    Handler handler = new Handler();
    final static int EXTERNAl_STORAGE_PERMISSION_REQUEST = 1001;
    final static String TAG = "MainActivity";
    // The TransferUtility is the primary class for managing transfer to S3
    private TransferUtility transferUtility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();

        AmazonS3Utils amazonS3Utils = new AmazonS3Utils();
        transferUtility = amazonS3Utils.getTransferUtility(this);

        /**
         * check and Resume all failed uploads
         */
        resumeFailedUploads(transferUtility,transferListener);

    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            if (screenShotCount < SCREENSHOT_LIMIT) {

                Bitmap screenshot = Utils.captureScreenShot(root_view);

                File file = Utils.saveScreenShotToStorage(screenshot);

                if (file != null) {

                    Log.d(TAG, screenShotCount+" Screenshot Captured & Saved to " + file.getAbsolutePath());

                    Toast.makeText(MainActivity.this, screenShotCount+" Screenshot Captured to " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();

                    startUploading(file);

                } else {

                    Log.d(TAG, "Failed to Capture Screenshot");

                }

                screenShotCount++;

                handler.postDelayed(runnable, SCREENSHOT_INTERVAL);

            }else {

                handler.removeCallbacks(runnable);

            }

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == EXTERNAl_STORAGE_PERMISSION_REQUEST){

            if (grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                // Start capturing screenshots...

                Log.d(TAG,"Permission Granted");

                startCapturingScreenshots();

                if (snackbar!=null){
                    snackbar.dismiss();
                }

            }else {

                snackbar =  Snackbar.make(coordinatorLayout,"You Need To Grant External Storage Permission ",Snackbar.LENGTH_INDEFINITE);

                snackbar.setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        snackbar.dismiss();

                        startCapturingScreenshots();

                    }
                });

                snackbar.show();

             //   ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},EXTERNAl_STORAGE_PERMISSION_REQUEST);

            }

        }

    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart");

        startCapturingScreenshots();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"onStop");

        handler.removeCallbacks(runnable);

        screenShotCount = 0;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"onResume");

    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.buttonClose){

            MainActivity.this.finish();

        }

    }

    private void initUI(){

        btClose = (Button)findViewById(R.id.buttonClose);
        root_view = (RelativeLayout)findViewById(R.id.activity_main);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        btClose.setOnClickListener(this);

    }

    private void startCapturingScreenshots(){

        // check Permissions

        if (Utils.checkExtStoragePermission(MainActivity.this)){

            Log.d(TAG,"Permission already granted");

            // start capturing screenshots

            handler.postDelayed(runnable,1000*5);    // delay 5 sec to start task

        }else {
            // ask for permission

            Log.d(TAG,"Permission not granted");

            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},EXTERNAl_STORAGE_PERMISSION_REQUEST);

        }
    }

    private void startUploading(File file){

        Log.d(TAG,"Uploading started "+file.getName());

        TransferObserver observer = transferUtility.upload(Constants.BUCKET_NAME, file.getName(), file);
        observer.setTransferListener(transferListener);

    }

    TransferListener transferListener = new TransferListener() {
        @Override
        public void onStateChanged(int id, TransferState state) {

            Log.d(TAG,"onStateChanged  id = "+id+ " state = "+state.name());

            if (TransferState.COMPLETED == state) {
                // Handle a completed upload.

                Toast.makeText(MainActivity.this, "ScreenShot Uploaded", Toast.LENGTH_SHORT).show();

            }
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
            int percentDone = (int)percentDonef;

            Log.d(TAG, "ID:" + id + " bytesCurrent: " + bytesCurrent
                    + " bytesTotal: " + bytesTotal + " " + percentDone + "%");

        }

        @Override
        public void onError(int id, Exception ex) {

            Log.d(TAG,"Error occured "+ex);

        }
    };

}
