package com.demo.screenshotapp;

import android.app.Application;
import android.support.multidex.MultiDex;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;

/**
 * Created by Ganesh on 4/14/2018.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        MultiDex.install(this);
        super.onCreate();

    }
}
