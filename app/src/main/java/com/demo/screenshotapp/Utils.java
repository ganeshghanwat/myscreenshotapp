package com.demo.screenshotapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferType;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ganesh on 4/14/2018.
 */

public class Utils {

    private static final String TAG = "Utils";

    static Bitmap captureScreenShot(View view){

        View rootView = view.getRootView();
        rootView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(rootView.getDrawingCache());
        rootView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    static File saveScreenShotToStorage(Bitmap bitmap){

        File myImageFile = null;

        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {

            String p = Environment.getExternalStorageDirectory().toString();

            File directory = new File(p,"/MyScreenShots");

            if (!directory.exists())
                directory.mkdirs();

            String fileName = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS",Locale.US).format(new Date()) + ".jpg";

             myImageFile = new File(directory,fileName);

            try {

                FileOutputStream out = new FileOutputStream(myImageFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();

            } catch (Exception e) {

                e.printStackTrace();

                Log.d(TAG,"Exception = "+e);

                myImageFile = null;

            }

        }else {

            Log.d(TAG,"External Storage Not Available/Writable");

            myImageFile = null;
        }

        return myImageFile;

    }

    static boolean checkExtStoragePermission(Context context){

        return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    static void resumeFailedUploads(TransferUtility transferUtility, TransferListener transferListener){

        TransferState[] states = {TransferState.FAILED,TransferState.WAITING_FOR_NETWORK};

        List<TransferObserver> transferObservers = transferUtility.getTransfersWithTypeAndStates(TransferType.UPLOAD,states);

        Log.d(TAG,"getTransfersWithTypeAndState Failed and Waiting for NW= "+transferObservers.size());

        for (TransferObserver observer : transferObservers){

            int id =  observer.getId();

            transferUtility.resume(id);

            observer.setTransferListener(transferListener);
        }
    }


}
